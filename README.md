# Red Hat UBI7

This repository mirrors Red Hat's UBI7 container image:

https://catalog.redhat.com/software/containers/ubi7/5c6cbf69dd19c77a158f2893

https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image

## Using this image with CERN mirrors

In order to use the CERN repository mirrors, you'll have to modify the repofile
as follows:

```dockerfile
FROM gitlab-registry.cern.ch/linuxsupport/ubi7/ubi

RUN sed -i 's#://cdn-ubi.redhat.com/#://linuxsoft.cern.ch/cdn-ubi.redhat.com/#' /etc/yum.repos.d/ubi.repo
```
